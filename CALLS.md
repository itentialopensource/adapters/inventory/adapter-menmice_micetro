## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Men and Mice Micetro. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Men and Mice Micetro.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Men and Mice Micetro. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createAddressSpace(body, callback)</td>
    <td style="padding:15px">Create Address Space</td>
    <td style="padding:15px">{base_path}/{version}/AddressSpaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressSpaces(callback)</td>
    <td style="padding:15px">Get Address Spaces</td>
    <td style="padding:15px">{base_path}/{version}/AddressSpaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAddressSpace(addressSpaceId, body, callback)</td>
    <td style="padding:15px">Update Address Space</td>
    <td style="padding:15px">{base_path}/{version}/AddressSpaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressSpaceById(addressSpaceId, callback)</td>
    <td style="padding:15px">Get Address Space By Id</td>
    <td style="padding:15px">{base_path}/{version}/AddressSpaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddressSpace(addressSpaceId, callback)</td>
    <td style="padding:15px">Delete Address Space</td>
    <td style="padding:15px">{base_path}/{version}/AddressSpaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createADForest(body, callback)</td>
    <td style="padding:15px">Create AD Forest</td>
    <td style="padding:15px">{base_path}/{version}/ADForests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADForests(callback)</td>
    <td style="padding:15px">Get AD Forests</td>
    <td style="padding:15px">{base_path}/{version}/ADForests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateADForest(adForestId, body, callback)</td>
    <td style="padding:15px">Update AD Forest</td>
    <td style="padding:15px">{base_path}/{version}/ADForests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADForestById(adForestId, callback)</td>
    <td style="padding:15px">Get AD Forest By Id</td>
    <td style="padding:15px">{base_path}/{version}/ADForests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteADForest(adForestId, callback)</td>
    <td style="padding:15px">Delete AD Forest</td>
    <td style="padding:15px">{base_path}/{version}/ADForests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createADSiteLink(body, callback)</td>
    <td style="padding:15px">Create AD SiteLink</td>
    <td style="padding:15px">{base_path}/{version}/ADSiteLinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADSiteLinks(callback)</td>
    <td style="padding:15px">Get AD Site Links</td>
    <td style="padding:15px">{base_path}/{version}/ADSiteLinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateADSiteLink(adSiteLinkId, body, callback)</td>
    <td style="padding:15px">Update AD Site Link</td>
    <td style="padding:15px">{base_path}/{version}/ADSiteLinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADSiteLinkById(adSiteLinkId, callback)</td>
    <td style="padding:15px">Get AD Site Link By Id</td>
    <td style="padding:15px">{base_path}/{version}/ADSiteLinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteADSiteLink(adSiteLinkId, callback)</td>
    <td style="padding:15px">Delete AD Site Link</td>
    <td style="padding:15px">{base_path}/{version}/ADSiteLinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createADSite(body, callback)</td>
    <td style="padding:15px">Create AD Site</td>
    <td style="padding:15px">{base_path}/{version}/ADSites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADSites(callback)</td>
    <td style="padding:15px">Get AD Sites</td>
    <td style="padding:15px">{base_path}/{version}/ADSites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateADSite(adSiteId, body, callback)</td>
    <td style="padding:15px">Update AD Site</td>
    <td style="padding:15px">{base_path}/{version}/ADSites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getADSiteById(adSiteId, callback)</td>
    <td style="padding:15px">Get AD Site By Id</td>
    <td style="padding:15px">{base_path}/{version}/ADSites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteADSite(adSiteId, callback)</td>
    <td style="padding:15px">Delete AD Site</td>
    <td style="padding:15px">{base_path}/{version}/ADSites/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChangeRequest(body, callback)</td>
    <td style="padding:15px">Create Change Request</td>
    <td style="padding:15px">{base_path}/{version}/ChangeRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeRequests(callback)</td>
    <td style="padding:15px">Get Change Requests</td>
    <td style="padding:15px">{base_path}/{version}/ChangeRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateChangeRequest(changeRequestId, body, callback)</td>
    <td style="padding:15px">Update Change Request</td>
    <td style="padding:15px">{base_path}/{version}/ChangeRequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeRequestById(changeRequestId, callback)</td>
    <td style="padding:15px">Get Change Request By Id</td>
    <td style="padding:15px">{base_path}/{version}/ChangeRequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteChangeRequest(changeRequestId, callback)</td>
    <td style="padding:15px">Delete Change Request</td>
    <td style="padding:15px">{base_path}/{version}/ChangeRequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudNetwork(body, callback)</td>
    <td style="padding:15px">Create Cloud Network</td>
    <td style="padding:15px">{base_path}/{version}/CloudNetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudNetworks(callback)</td>
    <td style="padding:15px">Get Cloud Networks</td>
    <td style="padding:15px">{base_path}/{version}/CloudNetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudNetwork(cloudNetworkId, body, callback)</td>
    <td style="padding:15px">Update Cloud Network</td>
    <td style="padding:15px">{base_path}/{version}/CloudNetworks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudNetworkById(cloudNetworkId, callback)</td>
    <td style="padding:15px">Get Cloud Network By Id</td>
    <td style="padding:15px">{base_path}/{version}/CloudNetworks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudNetwork(cloudNetworkId, callback)</td>
    <td style="padding:15px">Delete Cloud Network</td>
    <td style="padding:15px">{base_path}/{version}/CloudNetworks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudServiceAccount(body, callback)</td>
    <td style="padding:15px">Create Cloud Service Account</td>
    <td style="padding:15px">{base_path}/{version}/CloudServiceAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudServiceAccounts(callback)</td>
    <td style="padding:15px">Get Cloud Service Accounts</td>
    <td style="padding:15px">{base_path}/{version}/CloudServiceAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudServiceAccount(cloudServiceAccountId, body, callback)</td>
    <td style="padding:15px">Update Cloud Service Account</td>
    <td style="padding:15px">{base_path}/{version}/CloudServiceAccounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudServiceAccountById(cloudServiceAccountId, callback)</td>
    <td style="padding:15px">Get Cloud Service Account By Id</td>
    <td style="padding:15px">{base_path}/{version}/CloudServiceAccounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudServiceAccount(cloudServiceAccountId, callback)</td>
    <td style="padding:15px">Delete Cloud Service Account</td>
    <td style="padding:15px">{base_path}/{version}/CloudServiceAccounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevice(body, callback)</td>
    <td style="padding:15px">Create Device</td>
    <td style="padding:15px">{base_path}/{version}/Devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(callback)</td>
    <td style="padding:15px">Devices</td>
    <td style="padding:15px">{base_path}/{version}/Devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevice(deviceId, body, callback)</td>
    <td style="padding:15px">Update Device</td>
    <td style="padding:15px">{base_path}/{version}/Devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceById(deviceId, callback)</td>
    <td style="padding:15px">Get Device By Id</td>
    <td style="padding:15px">{base_path}/{version}/Devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevice(deviceId, callback)</td>
    <td style="padding:15px">Delete Device</td>
    <td style="padding:15px">{base_path}/{version}/Devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPAddressPool(body, callback)</td>
    <td style="padding:15px">Create DHCP Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/DHCPAddressPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPAddressPools(callback)</td>
    <td style="padding:15px">DHCP Address Pools</td>
    <td style="padding:15px">{base_path}/{version}/DHCPAddressPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPAddressPool(dhcpAddressPoolId, body, callback)</td>
    <td style="padding:15px">Update DHCP Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/DHCPAddressPools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPAddressPoolById(dhcpAddressPoolId, callback)</td>
    <td style="padding:15px">Get DHCP Address Pool By Id</td>
    <td style="padding:15px">{base_path}/{version}/DHCPAddressPools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPAddressPool(dhcpAddressPoolId, callback)</td>
    <td style="padding:15px">Delete DHCP Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/DHCPAddressPools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPExclusion(body, callback)</td>
    <td style="padding:15px">Create DHCP Exclusion</td>
    <td style="padding:15px">{base_path}/{version}/DHCPExclusions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPExclusions(callback)</td>
    <td style="padding:15px">DHCP Exclusions</td>
    <td style="padding:15px">{base_path}/{version}/DHCPExclusions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPExclusion(dhcpExclusionId, body, callback)</td>
    <td style="padding:15px">Update DHCP Exclusion</td>
    <td style="padding:15px">{base_path}/{version}/DHCPExclusions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPExclusionById(dhcpExclusionId, callback)</td>
    <td style="padding:15px">Get DHCP Exclusion By Id</td>
    <td style="padding:15px">{base_path}/{version}/DHCPExclusions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPExclusion(dhcpExclusionId, callback)</td>
    <td style="padding:15px">Delete DHCP Exclusion</td>
    <td style="padding:15px">{base_path}/{version}/DHCPExclusions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPGroup(body, callback)</td>
    <td style="padding:15px">Create DHCP Group</td>
    <td style="padding:15px">{base_path}/{version}/DHCPGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPGroups(callback)</td>
    <td style="padding:15px">DHCP Groups</td>
    <td style="padding:15px">{base_path}/{version}/DHCPGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPGroup(dhcpGroupId, body, callback)</td>
    <td style="padding:15px">Update DHCP Group</td>
    <td style="padding:15px">{base_path}/{version}/DHCPGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPGroupById(dhcpGroupId, callback)</td>
    <td style="padding:15px">Get DHCP Group By Id</td>
    <td style="padding:15px">{base_path}/{version}/DHCPGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPGroup(dhcpGroupId, callback)</td>
    <td style="padding:15px">Delete DHCP Group</td>
    <td style="padding:15px">{base_path}/{version}/DHCPGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPReservation(body, callback)</td>
    <td style="padding:15px">Create DHCP Reservation</td>
    <td style="padding:15px">{base_path}/{version}/DHCPReservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPReservations(callback)</td>
    <td style="padding:15px">DHCP Reservations</td>
    <td style="padding:15px">{base_path}/{version}/DHCPReservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPReservation(dhcpReservationId, body, callback)</td>
    <td style="padding:15px">Update DHCP Reservation</td>
    <td style="padding:15px">{base_path}/{version}/DHCPReservations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPReservationById(dhcpReservationId, callback)</td>
    <td style="padding:15px">Get DHCP Reservation By Id</td>
    <td style="padding:15px">{base_path}/{version}/DHCPReservations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPReservation(dhcpReservationId, callback)</td>
    <td style="padding:15px">Delete DHCP Reservation</td>
    <td style="padding:15px">{base_path}/{version}/DHCPReservations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPScope(body, callback)</td>
    <td style="padding:15px">Create DHCP Scope</td>
    <td style="padding:15px">{base_path}/{version}/DHCPScopes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPScopes(callback)</td>
    <td style="padding:15px">DHCP Scopes</td>
    <td style="padding:15px">{base_path}/{version}/DHCPScopes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPScope(dhcpScopeId, body, callback)</td>
    <td style="padding:15px">Update DHCP Scope</td>
    <td style="padding:15px">{base_path}/{version}/DHCPScopes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPScopeById(dhcpScopeId, callback)</td>
    <td style="padding:15px">Get DHCP Scope By Id</td>
    <td style="padding:15px">{base_path}/{version}/DHCPScopes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPScope(dhcpScopeId, callback)</td>
    <td style="padding:15px">Delete DHCP Scope</td>
    <td style="padding:15px">{base_path}/{version}/DHCPScopes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDHCPServer(body, callback)</td>
    <td style="padding:15px">Create DHCP Server</td>
    <td style="padding:15px">{base_path}/{version}/DHCPServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServers(callback)</td>
    <td style="padding:15px">DHCP Servers</td>
    <td style="padding:15px">{base_path}/{version}/DHCPServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPServer(dhcpServerId, body, callback)</td>
    <td style="padding:15px">Update DHCP Server</td>
    <td style="padding:15px">{base_path}/{version}/DHCPServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServerById(dhcpServerId, callback)</td>
    <td style="padding:15px">Get DHCP Server By Id</td>
    <td style="padding:15px">{base_path}/{version}/DHCPServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPServer(dhcpServerId, callback)</td>
    <td style="padding:15px">Delete DHCP Server</td>
    <td style="padding:15px">{base_path}/{version}/DHCPServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNSRecord(body, callback)</td>
    <td style="padding:15px">Create DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/DNSRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSRecords(callback)</td>
    <td style="padding:15px">DNS Records</td>
    <td style="padding:15px">{base_path}/{version}/DNSRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSRecord(dnsRecordId, body, callback)</td>
    <td style="padding:15px">Update DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/DNSRecords/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSRecordById(dnsRecordId, callback)</td>
    <td style="padding:15px">Get DNS Record By Id</td>
    <td style="padding:15px">{base_path}/{version}/DNSRecords/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSRecord(dnsRecordId, callback)</td>
    <td style="padding:15px">Delete DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/DNSRecords/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNSServer(body, callback)</td>
    <td style="padding:15px">Create DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/DNSServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServers(callback)</td>
    <td style="padding:15px">DNS Servers</td>
    <td style="padding:15px">{base_path}/{version}/DNSServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSServer(dnsServerId, body, callback)</td>
    <td style="padding:15px">Update DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/DNSServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerById(dnsServerId, callback)</td>
    <td style="padding:15px">Get DNS Server By Id</td>
    <td style="padding:15px">{base_path}/{version}/DNSServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSServer(dnsServerId, callback)</td>
    <td style="padding:15px">Delete DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/DNSServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNSView(body, callback)</td>
    <td style="padding:15px">Create DNS View</td>
    <td style="padding:15px">{base_path}/{version}/DNSViews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSViews(callback)</td>
    <td style="padding:15px">DNS Views</td>
    <td style="padding:15px">{base_path}/{version}/DNSViews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSView(dnsViewId, body, callback)</td>
    <td style="padding:15px">Update DNS View</td>
    <td style="padding:15px">{base_path}/{version}/DNSViews/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSViewById(dnsViewId, callback)</td>
    <td style="padding:15px">Get DNS View By Id</td>
    <td style="padding:15px">{base_path}/{version}/DNSViews/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSView(dnsViewId, callback)</td>
    <td style="padding:15px">Delete DNS View</td>
    <td style="padding:15px">{base_path}/{version}/DNSViews/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNSZone(body, callback)</td>
    <td style="padding:15px">Create DNS Zone</td>
    <td style="padding:15px">{base_path}/{version}/DNSZones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSZones(callback)</td>
    <td style="padding:15px">DNS Zones</td>
    <td style="padding:15px">{base_path}/{version}/DNSZones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSZone(dnsZoneId, body, callback)</td>
    <td style="padding:15px">Update DNS Zone</td>
    <td style="padding:15px">{base_path}/{version}/DNSZones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSZoneById(dnsZoneId, callback)</td>
    <td style="padding:15px">Get DNS Zone By Id</td>
    <td style="padding:15px">{base_path}/{version}/DNSZones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSZone(dnsZoneId, callback)</td>
    <td style="padding:15px">Delete DNS Zone</td>
    <td style="padding:15px">{base_path}/{version}/DNSZones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFolder(body, callback)</td>
    <td style="padding:15px">Create Folder</td>
    <td style="padding:15px">{base_path}/{version}/Folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFolders(callback)</td>
    <td style="padding:15px">Folders</td>
    <td style="padding:15px">{base_path}/{version}/Folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFolder(folderId, body, callback)</td>
    <td style="padding:15px">Update Folder</td>
    <td style="padding:15px">{base_path}/{version}/Folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFolderById(folderId, callback)</td>
    <td style="padding:15px">Get Folder By Id</td>
    <td style="padding:15px">{base_path}/{version}/Folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFolder(folderId, callback)</td>
    <td style="padding:15px">Delete Folder</td>
    <td style="padding:15px">{base_path}/{version}/Folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(body, callback)</td>
    <td style="padding:15px">Create Group</td>
    <td style="padding:15px">{base_path}/{version}/Groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(callback)</td>
    <td style="padding:15px">Groups</td>
    <td style="padding:15px">{base_path}/{version}/Groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(groupId, body, callback)</td>
    <td style="padding:15px">Update Group</td>
    <td style="padding:15px">{base_path}/{version}/Groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupById(groupId, callback)</td>
    <td style="padding:15px">Get Group By Id</td>
    <td style="padding:15px">{base_path}/{version}/Groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(groupId, callback)</td>
    <td style="padding:15px">Delete Group</td>
    <td style="padding:15px">{base_path}/{version}/Groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterface(body, callback)</td>
    <td style="padding:15px">Create Interface</td>
    <td style="padding:15px">{base_path}/{version}/Interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaces(callback)</td>
    <td style="padding:15px">Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/Interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInterface(interfaceId, body, callback)</td>
    <td style="padding:15px">Update Interface</td>
    <td style="padding:15px">{base_path}/{version}/Interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceById(interfaceId, callback)</td>
    <td style="padding:15px">Get Interface By Id</td>
    <td style="padding:15px">{base_path}/{version}/Interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterface(interfaceId, callback)</td>
    <td style="padding:15px">Delete Interface</td>
    <td style="padding:15px">{base_path}/{version}/Interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIPAMRecord(body, callback)</td>
    <td style="padding:15px">Create IPAM Record</td>
    <td style="padding:15px">{base_path}/{version}/IPAMRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMRecords(callback)</td>
    <td style="padding:15px">IPAM Records</td>
    <td style="padding:15px">{base_path}/{version}/IPAMRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIPAMRecord(ipamRecordId, body, callback)</td>
    <td style="padding:15px">Update IPAM Record</td>
    <td style="padding:15px">{base_path}/{version}/IPAMRecords/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMRecordById(ipamRecordId, callback)</td>
    <td style="padding:15px">Get IPAM Record By Id</td>
    <td style="padding:15px">{base_path}/{version}/IPAMRecords/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMRecord(ipamRecordId, callback)</td>
    <td style="padding:15px">Delete IPAM Record</td>
    <td style="padding:15px">{base_path}/{version}/IPAMRecords/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRange(body, callback)</td>
    <td style="padding:15px">Create Range</td>
    <td style="padding:15px">{base_path}/{version}/Ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRanges(callback)</td>
    <td style="padding:15px">Ranges</td>
    <td style="padding:15px">{base_path}/{version}/Ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRange(rangeId, body, callback)</td>
    <td style="padding:15px">Update Range</td>
    <td style="padding:15px">{base_path}/{version}/Ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRangeById(rangeId, callback)</td>
    <td style="padding:15px">Get Range By Id</td>
    <td style="padding:15px">{base_path}/{version}/Ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRange(rangeId, callback)</td>
    <td style="padding:15px">Delete Range</td>
    <td style="padding:15px">{base_path}/{version}/Ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReportDefinition(body, callback)</td>
    <td style="padding:15px">Create Report Definition</td>
    <td style="padding:15px">{base_path}/{version}/ReportDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportDefinitions(callback)</td>
    <td style="padding:15px">Report Definitions</td>
    <td style="padding:15px">{base_path}/{version}/ReportDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportDefinition(reportDefinitionId, body, callback)</td>
    <td style="padding:15px">Update Report Definition</td>
    <td style="padding:15px">{base_path}/{version}/ReportDefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportDefinitionById(reportDefinitionId, callback)</td>
    <td style="padding:15px">Get Report Definition By Id</td>
    <td style="padding:15px">{base_path}/{version}/ReportDefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportDefinition(reportDefinitionId, callback)</td>
    <td style="padding:15px">Delete Report Definition</td>
    <td style="padding:15px">{base_path}/{version}/ReportDefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReport(body, callback)</td>
    <td style="padding:15px">Create Report</td>
    <td style="padding:15px">{base_path}/{version}/Reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReports(callback)</td>
    <td style="padding:15px">Reports</td>
    <td style="padding:15px">{base_path}/{version}/Reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReport(reportId, body, callback)</td>
    <td style="padding:15px">Update Report</td>
    <td style="padding:15px">{base_path}/{version}/Reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportById(reportId, callback)</td>
    <td style="padding:15px">Get Report By Id</td>
    <td style="padding:15px">{base_path}/{version}/Reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReport(reportId, callback)</td>
    <td style="padding:15px">Delete Report</td>
    <td style="padding:15px">{base_path}/{version}/Reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReportSource(body, callback)</td>
    <td style="padding:15px">Create Report Source</td>
    <td style="padding:15px">{base_path}/{version}/ReportSources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportSources(callback)</td>
    <td style="padding:15px">Report Sources</td>
    <td style="padding:15px">{base_path}/{version}/ReportSources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportSource(reportSourceId, body, callback)</td>
    <td style="padding:15px">Update Report Source</td>
    <td style="padding:15px">{base_path}/{version}/ReportSources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportSourceById(reportSourceId, callback)</td>
    <td style="padding:15px">Get Report Source By Id</td>
    <td style="padding:15px">{base_path}/{version}/ReportSources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportSource(reportSourceId, callback)</td>
    <td style="padding:15px">Delete Report Source</td>
    <td style="padding:15px">{base_path}/{version}/ReportSources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRole(body, callback)</td>
    <td style="padding:15px">Create Role</td>
    <td style="padding:15px">{base_path}/{version}/Roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(callback)</td>
    <td style="padding:15px">Roles</td>
    <td style="padding:15px">{base_path}/{version}/Roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRole(roleId, body, callback)</td>
    <td style="padding:15px">Update Role</td>
    <td style="padding:15px">{base_path}/{version}/Roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleById(roleId, callback)</td>
    <td style="padding:15px">Get Role By Id</td>
    <td style="padding:15px">{base_path}/{version}/Roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRole(roleId, callback)</td>
    <td style="padding:15px">Delete Role</td>
    <td style="padding:15px">{base_path}/{version}/Roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create User</td>
    <td style="padding:15px">{base_path}/{version}/Users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">Users</td>
    <td style="padding:15px">{base_path}/{version}/Users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(userId, body, callback)</td>
    <td style="padding:15px">Update User</td>
    <td style="padding:15px">{base_path}/{version}/Users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserById(userId, callback)</td>
    <td style="padding:15px">Get User By Id</td>
    <td style="padding:15px">{base_path}/{version}/Users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(userId, callback)</td>
    <td style="padding:15px">Delete User</td>
    <td style="padding:15px">{base_path}/{version}/Users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
