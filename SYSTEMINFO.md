# Men and Mice Micetro

Vendor: Men and Mice Micetro
Homepage: https://www.menandmice.com/

Product: Men and Mice Micetro
Product Page: https://www.menandmice.com/products/micetro

## Introduction
We classify Men and Mice Micetro into the Inventory and Network Services domains as Men and Mice Micetro provides a solution for managing DNS, DHCP, and IP address allocation in order to streamline network management processes.

## Why Integrate
The Men and Mice Micetro adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Men and Mice Micetro. With this adapter you have the ability to perform operations on items such as:

- IPAM
- DHCP
- DNS

## Additional Product Documentation
The [API Guide for Men and Mice Micetro](https://docs.menandmice.com/en/latest/guides/user-manual/rest_api/)